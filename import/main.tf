
# resource "aws_vpc" "vpc" {
#     cidr_block = "10.0.0.0/16"
#     tags = {
#         Name = "Original VPC"
#     }
# }

resource "aws_vpc" "myvpc" {
    cidr_block = "10.0.0.0/16"
    tags = {
        Name = "Imported VPC"
    }
}

# resource "aws_instance" "myec2" {
#     ami = "ami-087c17d1fe0178315"
#     instance_type = "t2.micro"
# }


