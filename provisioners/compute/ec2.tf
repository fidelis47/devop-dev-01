resource "aws_instance" "dev-ec2" {
    ami = var.ami
    instance_type = var.type
    tags = var.tags
    key_name = "provisioners"
    availability_zone = var.az

    vpc_security_group_ids = [aws_security_group.allow_ssh.id]

    provisioner "remote-exec" {
      inline = [
        "sudo amazon-linux-extras install -y nginx1.12",
        "sudo systemctl start nginx"
      ]

    connection {
      type = "ssh"
      user = "ec2-user"
      private_key = file("./provisioners.pem")
      host = self.public_ip 
    }  
    }
}

### NOTE - Adding a new security group resource to allow the terraform provisioner from laptop to connect to EC2 Instance via SSH.

resource "aws_security_group" "allow_ssh" {
  name        = "allow_ssh"
  description = "Allow SSH inbound traffic"

  ingress {
    description = "SSH into VPC"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    description = "Outbound Allowed"
    from_port   = 0
    to_port     = 65535
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
        from_port = var.httpport
        to_port = var.httpport
        protocol = var.tcp
        cidr_blocks = ["0.0.0.0/0"]
    }

    egress {
        from_port = var.httpport
        to_port = var.httpport
        protocol = var.tcp
        cidr_blocks = ["0.0.0.0/0"]
    }
}

resource "aws_ebs_volume" "dev-vol" {
  availability_zone = var.az
  size              = var.volsize
  tags              = var.tags
}

resource "aws_volume_attachment" "dev-vol-att" {
  device_name = var.devname
  volume_id   = aws_ebs_volume.dev-vol.id
  instance_id = aws_instance.dev-ec2.id
}

